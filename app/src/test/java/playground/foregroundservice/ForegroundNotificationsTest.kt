package playground.foregroundservice

import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ForegroundNotificationsTest {

    private lateinit var foregroundServiceHelper: ForegroundServiceHelper
    private lateinit var notificationService: NotificationService
    private lateinit var foregroundNotifications: ForegroundNotifications

    @Before
    fun setUp() {
        foregroundServiceHelper = mock()
        notificationService = mock()
        foregroundNotifications = ForegroundNotifications(
            mock(),
            foregroundServiceHelper,
            notificationService
        )
    }

    @Test
    fun whenNotifyAndDetachPreviousThenItShouldUpdateNotificationIds() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        assertTrue(foregroundNotifications.notificationIds.contains(1))
        assertTrue(foregroundNotifications.notificationIds.contains(2))
        assertTrue(foregroundNotifications.notificationIds.contains(3))
    }

    @Test
    fun whenNotifyAndDetachPreviousThenItShouldUpdateActiveNotificationId() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        assertEquals(3, foregroundNotifications.activeNotificationId)
    }

    @Test
    fun whenCancelThenItShouldUpdateNotificationIds() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        foregroundNotifications.cancel(2)

        assertTrue(foregroundNotifications.notificationIds.contains(1))
        assertFalse(foregroundNotifications.notificationIds.contains(2))
        assertTrue(foregroundNotifications.notificationIds.contains(3))
    }

    @Test
    fun whenCancelNonActiveNotificationThenItShouldNotUpdateActiveNotificationId() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        foregroundNotifications.cancel(1)

        assertEquals(3, foregroundNotifications.activeNotificationId)
    }

    @Test
    fun whenCancelNonActiveNotificationThenItShouldNotTriggerNewNotification() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        foregroundNotifications.cancel(1)

        // 1 time: in notifyAndDetachPrevious(2, mock()).
        verify(
            foregroundServiceHelper,
            times(1)
        ).startForegroundAndDetachPreviousNotification(eq(2), any())
    }

    @Test
    fun whenCancelActiveNotificationThenItShouldUpdateActiveNotificationId() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        foregroundNotifications.cancel(3)

        assertEquals(2, foregroundNotifications.activeNotificationId)
    }

    @Test
    fun whenCancelActiveNotificationThenItShouldTriggerNewNotification() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.notifyAndDetachPrevious(2, mock())
        foregroundNotifications.notifyAndDetachPrevious(3, mock())

        foregroundNotifications.cancel(3)

        // 2 times: first in notifyAndDetachPrevious(2, mock()) and second in cancel(3).
        verify(
            foregroundServiceHelper,
            times(2)
        ).startForegroundAndDetachPreviousNotification(eq(2), any())
    }

    @Test
    fun whenEmptyThenItShouldCorrectlyIndicateThat() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())
        foregroundNotifications.cancel(1)

        assertTrue(foregroundNotifications.isEmpty)
    }

    @Test
    fun whenNotEmptyThenItShouldCorrectlyIndicateThat() {
        foregroundNotifications.notifyAndDetachPrevious(1, mock())

        assertFalse(foregroundNotifications.isEmpty)
    }

}