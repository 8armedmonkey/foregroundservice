package playground.foregroundservice

enum class Action(val id: Int) {
    Foo(1),
    Bar(2),
    Baz(3)
}