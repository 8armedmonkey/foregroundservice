package playground.foregroundservice

import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        NotificationServiceImpl.createDefaultChannel(this)
    }

}