package playground.foregroundservice

import android.app.Notification
import android.content.Context

class ForegroundNotifications(
    private val context: Context,
    private val foregroundServiceHelper: ForegroundServiceHelper,
    private val notificationService: NotificationService
) {

    var activeNotificationId: Int? = null
        private set

    val notificationIds: Set<Int>
        get() = entries.keys

    val isEmpty: Boolean
        get() = entries.isEmpty()

    private val entries: LinkedHashMap<Int, Notification> = LinkedHashMap()

    fun notifyAndDetachPrevious(notificationId: Int, notification: Notification) {
        synchronized(this) {
            foregroundServiceHelper.startForegroundAndDetachPreviousNotification(
                notificationId,
                notification
            )

            entries[notificationId] = notification
            activeNotificationId = notificationId
        }
    }

    fun cancel(notificationId: Int) {
        synchronized(this) {
            if (notificationId == activeNotificationId) {
                val newActiveNotificationId = entries.keys.findLast { it != activeNotificationId }
                val notification = entries[newActiveNotificationId]

                if (newActiveNotificationId != null && notification != null) {
                    notifyAndDetachPrevious(newActiveNotificationId, notification)
                }
            }

            entries.remove(notificationId)

            if (isEmpty) {
                foregroundServiceHelper.stopForeground()
            } else {
                notificationService.cancel(context, id = notificationId)
            }
        }
    }

}