package playground.foregroundservice

import android.app.Notification

interface ForegroundServiceHelper {

    fun startForegroundAndDetachPreviousNotification(
        notificationId: Int,
        notification: Notification
    )

    fun stopForeground()

    fun stopSelf()

}