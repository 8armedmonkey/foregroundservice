package playground.foregroundservice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(foo_check_box) {
            setOnClickListener {
                startService(
                    ForegroundService.getStartIntent(
                        this@MainActivity,
                        Action.Foo,
                        isChecked
                    )
                )
            }
        }

        with(bar_check_box) {
            setOnClickListener {
                startService(
                    ForegroundService.getStartIntent(
                        this@MainActivity,
                        Action.Bar,
                        isChecked
                    )
                )
            }
        }

        with(baz_check_box) {
            setOnClickListener {
                startService(
                    ForegroundService.getStartIntent(
                        this@MainActivity,
                        Action.Baz,
                        isChecked
                    )
                )
            }
        }
    }
}