package playground.foregroundservice

import android.app.Service
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService

class ForegroundService : LifecycleService() {

    private val foregroundNotifications: ForegroundNotifications by lazy {
        ForegroundNotifications(
            context = this,
            foregroundServiceHelper = ForegroundServiceHelperImpl(this),
            notificationService = NotificationServiceImpl
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        val action = intent?.getSerializableExtra(EXTRA_ACTION) as Action?
        val enabled = intent?.getBooleanExtra(EXTRA_ENABLED, false)

        if (action != null) {
            if (enabled == true) {
                showNotification(action)
            } else {
                cancelNotification(action)
            }
        }
        return Service.START_STICKY
    }

    private fun showNotification(action: Action) {
        val notification =
            NotificationCompat.Builder(this, NotificationServiceImpl.DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setStyle(NotificationCompat.BigTextStyle().bigText(action.name))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(action.name)
                .setTicker(action.name)
                .setOngoing(true)
                .setAutoCancel(false)
                .setGroup(NotificationServiceImpl.ACTIVE_EVENTS_GROUP_ID)
                .build()

        foregroundNotifications.notifyAndDetachPrevious(action.id, notification)
    }

    private fun cancelNotification(action: Action) {
        foregroundNotifications.cancel(action.id)
    }

    companion object {

        private const val EXTRA_ACTION = "action"
        private const val EXTRA_ENABLED = "enabled"

        fun getStartIntent(context: Context, action: Action, enabled: Boolean): Intent =
            Intent(context, ForegroundService::class.java).apply {
                putExtra(EXTRA_ACTION, action)
                putExtra(EXTRA_ENABLED, enabled)
            }

    }
}