package playground.foregroundservice

import android.app.Notification
import android.content.Context

interface NotificationService {

    fun createDefaultChannel(context: Context)

    fun notify(context: Context, tag: String? = null, id: Int, notification: Notification)

    fun cancel(context: Context, tag: String? = null, id: Int)

}