package playground.foregroundservice

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationManagerCompat

object NotificationServiceImpl : NotificationService {

    const val DEFAULT_CHANNEL_ID = "default"
    const val ACTIVE_EVENTS_GROUP_ID = "active-events"

    override fun createDefaultChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            with(NotificationManagerCompat.from(context)) {
                if (!notificationChannelExists(context, DEFAULT_CHANNEL_ID)) {
                    createNotificationChannel(
                        NotificationChannel(
                            DEFAULT_CHANNEL_ID,
                            context.getString(R.string.default_notification_channel),
                            NotificationManager.IMPORTANCE_DEFAULT
                        )
                    )
                }
            }
        }
    }

    override fun notify(context: Context, tag: String?, id: Int, notification: Notification) {
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .notify(tag, id, notification)
    }

    override fun cancel(context: Context, tag: String?, id: Int) {
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .cancel(tag, id)
    }

    @Suppress("SameParameterValue")
    private fun notificationChannelExists(context: Context, channelId: String): Boolean =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManagerCompat.from(context).getNotificationChannel(channelId) != null
        } else {
            false
        }

}