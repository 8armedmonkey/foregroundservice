package playground.foregroundservice

import android.app.Notification
import android.app.Service
import androidx.core.app.ServiceCompat

class ForegroundServiceHelperImpl(
    private val service: Service
) : ForegroundServiceHelper {

    override fun startForegroundAndDetachPreviousNotification(
        notificationId: Int,
        notification: Notification
    ) {
        ServiceCompat.stopForeground(service, ServiceCompat.STOP_FOREGROUND_DETACH)
        service.startForeground(notificationId, notification)
    }

    override fun stopForeground() {
        ServiceCompat.stopForeground(service, ServiceCompat.STOP_FOREGROUND_REMOVE)
    }

    override fun stopSelf() {
        service.stopSelf()
    }

}